package com.o2.testpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class O2TestChat {
	
	@Test
	public void runTest() throws InterruptedException {
		
		String userNameandPwd = "priyakhare123";
		
		System.setProperty("webdriver.chrome.driver","chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://login.o2online.de/auth/login?goto=https%3A%2F%2Fwww.o2online.de%2Fmein-o2%2F&_gl=1*oo2zi3*_gcl_au*MjA5NzAyNDg5NC4xNjkxNjEwOTg4");
		driver.manage().window().maximize();
		Thread.sleep(6000);
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		WebElement acceptCookie = (WebElement)js.executeScript("return document.querySelector(\"#usercentrics-root\").shadowRoot.querySelector(\"#uc-center-container > div > div.sc-iTVJFM.dIhOzs > div > div > div > button:nth-child(3)\")");
		
		js.executeScript("arguments[0].click();", acceptCookie);
		
		driver.findElement(By.xpath("//input[@id='IDToken1']")).sendKeys(userNameandPwd);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@name='IDButton']")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.id("IDToken1")).sendKeys(userNameandPwd);
		Thread.sleep(2000);
		driver.findElement(By.id("login-password-submit-button")).click();
		Thread.sleep(2000);
		String text = driver.findElement(By.xpath("//h1[@class='brand-hero-title content--centered qa-brand-hero-title']")).getText();
		System.out.println(text);
		driver.get("https://www.o2online.de/service/");
		driver.quit();
	}


}
